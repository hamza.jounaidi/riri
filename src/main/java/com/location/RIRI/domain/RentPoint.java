package com.location.RIRI.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

/**
 * The persistent class for the role database table.
 */
@Entity
@NamedQuery(name = "RentPoint.findAll", query = "SELECT r FROM RentPoint r")
public class RentPoint implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_rent_point")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rent_point_generator")
	@SequenceGenerator(name="rent_point_generator", sequenceName = "rent_point_seq", initialValue = 1)
    private Integer idRentPoint;

    @Column(name = "label")
    private String label;

    @Column(name = "adress")
    private String adress;

    //bi-directional many-to-one association to User
    @OneToMany(mappedBy = "rentPoint")
    private List<Item> items;

    public Integer getIdRentPoint() {
		return idRentPoint;
	}

	public void setIdRentPoint(Integer idRentPoint) {
		this.idRentPoint = idRentPoint;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public List<Item> getItems() {
        return (this.items != null) ? new ArrayList<Item>(this.items) : null;
    }

    public void setItems(List<Item> items) {
        this.items = new ArrayList<Item>();
        if (items != null) {
            this.items.addAll(items);
        }
    }

    public Item addItem(Item item) {
    	item.setRentPoint(this);
        this.items.add(item);

        return item;
    }

    public Item removeItem(Item item) {
        getItems().remove(item);
        item.setRentPoint(null);

        return item;
    }
}