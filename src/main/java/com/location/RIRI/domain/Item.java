package com.location.RIRI.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the users database table.
 */
@Entity
@Table(name = "item")
@NamedQuery(name = "Item.findAll", query = "SELECT i FROM Item i")
public class Item implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_item")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "item_generator")
	@SequenceGenerator(name="item_generator", sequenceName = "item_seq", initialValue = 1)
    private Integer idItem;

    @Column(name = "label")
    private String label;

    @Column(name = "price_by_day")
    private BigDecimal priceByDay;

    @Column(name = "garentee")
    private BigDecimal garentee;

    @Column(name = "penalty_by_day")
    private BigDecimal penaltyByDay;

    @Column(name = "description")
    private String description;

    // bi-directional many-to-one association to rentPoint
    @ManyToOne
    @JoinColumn(name = "id_rent_point")
    private RentPoint rentPoint;

    // bi-directional many-to-one association to Wishlist
    @OneToMany(mappedBy = "item")
    private List<Wishlist> wishlists;

    // bi-directional many-to-one association to RentedItem
    @OneToMany(mappedBy = "item")
    private List<RentedItem> rentedItems;

    

    public Integer getIdItem() {
		return idItem;
	}

	public void setIdItem(Integer idItem) {
		this.idItem = idItem;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public BigDecimal getPriceByDay() {
		return priceByDay;
	}

	public void setPriceByDay(BigDecimal priceByDay) {
		this.priceByDay = priceByDay;
	}

	public BigDecimal getGarentee() {
		return garentee;
	}

	public void setGarentee(BigDecimal garentee) {
		this.garentee = garentee;
	}

	public BigDecimal getPenaltyByDay() {
		return penaltyByDay;
	}

	public void setPenaltyByDay(BigDecimal penaltyByDay) {
		this.penaltyByDay = penaltyByDay;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public RentPoint getRentPoint() {
		return rentPoint;
	}

	public void setRentPoint(RentPoint rentPoint) {
		this.rentPoint = rentPoint;
	}

	public List<Wishlist> getWishlists() {
        return (this.wishlists != null) ? new ArrayList<Wishlist>(this.wishlists) : null;
    }

    public void setWishlists(List<Wishlist> wishlists) {
        this.wishlists = new ArrayList<Wishlist>();
        if (wishlists != null) {
            this.wishlists.addAll(wishlists);
        }
    }

    public Wishlist addWishlist(Wishlist wishlist) {
    	wishlist.setItem(this);
        this.wishlists.add(wishlist);

        return wishlist;
    }

    public Wishlist removeWishlist(Wishlist wishList) {
        getWishlists().remove(wishList);
        wishList.setUser(null);

        return wishList;
    }

    public List<RentedItem> getRentedItems() {
        return (this.rentedItems != null) ? new ArrayList<RentedItem>(this.rentedItems) : null;
    }

    public void setRentedItems(List<RentedItem> rentedItems) {
        this.rentedItems = new ArrayList<RentedItem>();
        if (rentedItems != null) {
            this.rentedItems.addAll(rentedItems);
        }
    }

    public RentedItem addRentedItem(RentedItem rentedItem) {
    	rentedItem.setItem(this);
        this.rentedItems.add(rentedItem);

        return rentedItem;
    }

    public RentedItem removeRentedItem(RentedItem rentedItem) {
        getRentedItems().remove(rentedItem);
        rentedItem.setUser(null);

        return rentedItem;
    }

}
