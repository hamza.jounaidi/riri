package com.location.RIRI.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The persistent class for the role database table.
 */
@Entity
@NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r")
public class Role implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_role")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idRole;

    @Column(name = "role_name")
    private String roleName;

    //bi-directional many-to-one association to User
    @OneToMany(mappedBy = "role")
    private List<User> users;

    public Integer getIdRole() {
        return this.idRole;
    }

    public void setIdRole(Integer idRole) {
        this.idRole = idRole;
    }

    public String getRoleName() {
        return this.roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<User> getUsers() {
        return (this.users != null) ? new ArrayList<User>(this.users) : null;
    }

    public void setUsers(List<User> users) {
        this.users = new ArrayList<User>();
        if (users != null) {
            this.users.addAll(users);
        }
    }

    public User addUser(User user) {
        user.setRole(this);
        this.users.add(user);

        return user;
    }

    public User removeUser(User user) {
        getUsers().remove(user);
        user.setRole(null);

        return user;
    }

    @Override
    public String toString() {
        return "Role{" +
                "idRole=" + idRole +
                ", roleName='" + roleName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Role role = (Role) o;
        return Objects.equals(idRole, role.idRole) &&
                Objects.equals(roleName, role.roleName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idRole, roleName, users);
    }
}