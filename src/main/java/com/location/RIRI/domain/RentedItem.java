package com.location.RIRI.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the rentedItem database table.
 */
@Entity
@Table(name = "rented_item")
@NamedQuery(name = "RentedItem.findAll", query = "SELECT r FROM RentedItem r")
public class RentedItem implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_rented_item")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rented_item_generator")
	@SequenceGenerator(name="rented_item_generator", sequenceName = "rented_item_seq", initialValue = 1)
    private Integer idRentedItem;

    @Column(name = "reservation_date")
    private Timestamp reservationDate;

    @Column(name = "rent_date")
    private Timestamp rentDate;

    @Column(name = "return_date")
    private Timestamp returnDate;

    @Column(name = "rent_duration")
    private Integer rentDuration;

    // bi-directional many-to-one association to User
    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    // bi-directional many-to-one association to Item
    @ManyToOne
    @JoinColumn(name = "id_item")
    private Item item;

    public Integer getIdRentedItem() {
        return this.idRentedItem;
    }

    public void setIdRentedItem(Integer idRentedItem) {
        this.idRentedItem = idRentedItem;
    }

	public Timestamp getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(Timestamp reservationDate) {
		this.reservationDate = reservationDate;
	}

	public Timestamp getRentDate() {
		return rentDate;
	}

	public void setRentDate(Timestamp rentDate) {
		this.rentDate = rentDate;
	}

	public Timestamp getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Timestamp returnDate) {
		this.returnDate = returnDate;
	}

	public Integer getRentDuration() {
		return rentDuration;
	}

	public void setRentDuration(Integer rentDuration) {
		this.rentDuration = rentDuration;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

}
