package com.location.RIRI.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the wishlist database table.
 */
@Entity
@Table(name = "wishlist")
@NamedQuery(name = "Wishlist.findAll", query = "SELECT w FROM Wishlist w")
public class Wishlist implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_wishlist")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wishlist_generator")
	@SequenceGenerator(name="wishlist_generator", sequenceName = "wishlist_seq", initialValue = 1)
    private Integer idWishlist;

    // bi-directional many-to-one association to User
    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    // bi-directional many-to-one association to Item
    @ManyToOne
    @JoinColumn(name = "id_item")
    private Item item;

    public Integer getIdWishlist() {
        return this.idWishlist;
    }

    public void setIdWishlist(Integer idWishlist) {
        this.idWishlist = idWishlist;
    }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

}
