package com.location.RIRI.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the users database table.
 */
@Entity
@Table(name = "user")
@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_user")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
	@SequenceGenerator(name="user_generator", sequenceName = "user_seq", initialValue = 1)
    private Integer idUser;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "mail")
    private String mail;

    // bi-directional many-to-one association to Role
    @ManyToOne
    @JoinColumn(name = "id_role")
    private Role role;

    // bi-directional many-to-one association to Wishlist
    @OneToMany(mappedBy = "user")
    private List<Wishlist> wishlists;

    // bi-directional many-to-one association to RentedItem
    @OneToMany(mappedBy = "user")
    private List<RentedItem> rentedItems;

    public Integer getIdUser() {
        return this.idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Role getRole() {
        return this.role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Wishlist> getWishlists() {
        return (this.wishlists != null) ? new ArrayList<Wishlist>(this.wishlists) : null;
    }

    public void setWishlists(List<Wishlist> wishlists) {
        this.wishlists = new ArrayList<Wishlist>();
        if (wishlists != null) {
            this.wishlists.addAll(wishlists);
        }
    }

    public Wishlist addWishlist(Wishlist wishlist) {
    	wishlist.setUser(this);
        this.wishlists.add(wishlist);

        return wishlist;
    }

    public Wishlist removeWishlist(Wishlist wishlist) {
        getWishlists().remove(wishlist);
        wishlist.setUser(null);

        return wishlist;
    }

    public List<RentedItem> getRentedItems() {
        return (this.rentedItems != null) ? new ArrayList<RentedItem>(this.rentedItems) : null;
    }

    public void setRentedItems(List<RentedItem> rentedItems) {
        this.rentedItems = new ArrayList<RentedItem>();
        if (rentedItems != null) {
            this.rentedItems.addAll(rentedItems);
        }
    }

    public RentedItem addRentedItem(RentedItem rentedItem) {
    	rentedItem.setUser(this);
        this.rentedItems.add(rentedItem);

        return rentedItem;
    }

    public RentedItem removeRentedItem(RentedItem rentedItem) {
        getRentedItems().remove(rentedItem);
        rentedItem.setUser(null);

        return rentedItem;
    }

}
